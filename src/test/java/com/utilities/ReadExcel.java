package com.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.*;

public class ReadExcel {
	
	public static String [][] connectExcelFile(String fileName, String sheetName) throws InvalidFormatException, IOException
	{
		File file = new File(System.getProperty("user.dir")+"//TestData//"+fileName);
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		XSSFRow row = sheet.getRow(0);
		XSSFCell cell;
		int totalRows=sheet.getLastRowNum();
		int totalCols=row.getLastCellNum();
		
		String getExcelData [] [] = new String [totalRows][totalCols];
		for(int rows=1;rows<=totalRows;rows++)
		{
			for (int cols = 0; cols < totalCols; cols++) 
			{
				row = sheet.getRow(rows);
				cell = row.getCell(cols);
				getExcelData[rows-1][cols]=cell.toString();
//				System.out.print(cell.toString()+"\t");
			}
			System.out.println("");
		}
		for (int i = 0; i < totalRows; i++) 
		{
			for (int j = 0; j < totalCols-1; j++) 
			{
				System.out.print(getExcelData[i][j]+"\t");
			}
			System.out.println();
		}
		return getExcelData;
	}	
}
