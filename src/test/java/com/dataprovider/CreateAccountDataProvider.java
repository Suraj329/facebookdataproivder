package com.dataprovider;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.DataProvider;
import com.utilities.ReadExcel;

public class CreateAccountDataProvider {
	
	@DataProvider(name = "Create Account")
	public String [][] getCreateAccountData() throws InvalidFormatException, IOException
	{
		return ReadExcel.connectExcelFile("Book1.xlsx", "Sheet1");
	}
}
