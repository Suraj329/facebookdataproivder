package com.wrapper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Elements {
	
	private static WebDriver wd;
	private static WebElement element;
	private static Select select;
	
	public static void openBrowser(String broweserName)
	{
		
		if (broweserName.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			wd = new ChromeDriver();
		}
		else if (broweserName.equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			wd = new ChromeDriver();
		}
		else
		{
			System.out.println("type Chrome or Firefox only");
		}
	}
		
	public static void doClick(By locator)
	{
		element= wd.findElement(locator);
		element.click();
	}
	
	public static void doSendText(By locator, String text)
	{
			element=wd.findElement(locator);
			element.sendKeys(text);
	}
	
	public static void doSelectDropdownByVisibleText(By selectLocator, String visibleText)
	{
			select = new Select(wd.findElement(selectLocator));
			select.selectByVisibleText(visibleText);
	}
	
	public static void doSelectDropdownByValue(String value,By selectLocator)
	{
			select = new Select(wd.findElement(selectLocator));
			select.selectByValue(value);
	}
	
	public static void doSelectDropdownByIndex(int index,By selectLocator)
	{
			select = new Select(wd.findElement(selectLocator));
			select.selectByIndex(index);
	}
	
	public static void openUrl(String url)
	{
		wd.get(url);
	}
}
