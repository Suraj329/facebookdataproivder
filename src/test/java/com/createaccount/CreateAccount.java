package com.createaccount;

import org.openqa.selenium.By;

import com.wrapper.Elements;

public final class CreateAccount {
	
	private static final By CREATE_ACCOUNT_BUTTON_LOCATOR = By.xpath("//a[@class='_42ft _4jy0 _6lti _4jy6 _4jy2 selected _51sy']");
	private static final By FIRST_NAME_TEXTBOX_LOCATOR = By.name("firstname");
	private static final By LAST_NAME_TEXTBOX_LOCATOR = By.name("lastname");
	private static final By EMAILID_TEXTBOX_LOCATOR = By.name("reg_email__");
	private static final By REEMAILID_TEXTBOX_LOCATOR = By.name("reg_email_confirmation__");
	private static final By PASSWORD_TEXTBOX_LOCATOR = By.name("reg_passwd__");
	private static final By BIRTHDAY_DROPDOWN_LOCATOR = By.xpath("//select[@name='birthday_day']");
	private static final By BIRTHMONTH_DROPDOWN_LOCATOR = By.xpath("//select[@name='birthday_month']");
	private static final By BIRTHYEAR_DROPDOWN_LOCATOR = By.xpath("//select[@name='birthday_year']");
	private static final By FEMALE_RADIOBUTTON_LOCATOR = By.xpath("//input[@value='1']");
	private static final By MALE_RADIOBUTTON_LOCATOR = By.xpath("//input[@value='2']");
	private static final By CUSTOM_RADIOBUTTON_LOCATOR = By.xpath("//input[@value='-1']");
	private static final By SIGNUP_BUTTON_LOCATOR = By.xpath("//button[@name='websubmit']");

	public static void doCreateAccount(String firstName, String lastName, String emailId, String reEmailId, String password, String birthDay, String birthMonth, String birthYear, String sex) throws InterruptedException
	{
		Elements.doClick(CREATE_ACCOUNT_BUTTON_LOCATOR);
		Thread.sleep(2000);
		Elements.doSendText(FIRST_NAME_TEXTBOX_LOCATOR, firstName);
		Elements.doSendText(LAST_NAME_TEXTBOX_LOCATOR, lastName);
		Elements.doSendText(EMAILID_TEXTBOX_LOCATOR, emailId);
		Elements.doSendText(REEMAILID_TEXTBOX_LOCATOR, reEmailId);
		Elements.doSendText(PASSWORD_TEXTBOX_LOCATOR, password);
		Elements.doSelectDropdownByVisibleText(BIRTHDAY_DROPDOWN_LOCATOR, birthDay);
		Elements.doSelectDropdownByIndex(Integer.parseInt(birthMonth), BIRTHMONTH_DROPDOWN_LOCATOR);
		Elements.doSelectDropdownByVisibleText(BIRTHYEAR_DROPDOWN_LOCATOR, birthYear);
		if(sex.equalsIgnoreCase("M"))
		{
			Elements.doClick(MALE_RADIOBUTTON_LOCATOR);
		}
		else if(sex.equalsIgnoreCase("F"))
		{
			Elements.doClick(FEMALE_RADIOBUTTON_LOCATOR);
		}
		else if(sex.equalsIgnoreCase("C"))
		{
			Elements.doClick(CUSTOM_RADIOBUTTON_LOCATOR);
		}
		else
		{
			System.out.println("Wrong Sex Entered");
		}
		Elements.doClick(SIGNUP_BUTTON_LOCATOR);
	}
}
