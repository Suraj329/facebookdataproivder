package com.createaccount.test;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.createaccount.CreateAccount;
import com.wrapper.Elements;

public class CreateAccountTest {
	
	@BeforeMethod
	public void openBrowser()
	{
		Elements.openBrowser("Chrome");
		Elements.openUrl("https://www.facebook.com/");
	}
	@Test(testName = "Create Account" , description = "Verify Facebook Create Account", dataProviderClass = com.dataprovider.CreateAccountDataProvider.class , dataProvider = "Create Account")
	public void runCreateAccount(String firstName, String lastName, String emailId, String reEmailId, String password, String birthDay, String birthMonth, String birthYear, String sex) throws InterruptedException
	{
		CreateAccount.doCreateAccount(firstName, lastName, emailId, reEmailId, password, birthDay, birthMonth, birthYear, sex);
		Thread.sleep(6000);
		Assert.assertEquals("A","A");
	}
}
